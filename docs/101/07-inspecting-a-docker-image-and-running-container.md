# Inspecting a docker image and running container

To get more information on a given container or image we can use the `docker inspect` command.

This displays the low-level information of the container or image in a JSON array (e.g. the IP address or the image layers).
 
Docker inspect takes the ID of the object you want to inspect as a parameter.

## Inspecting the image 

Firstly execute the following command to obtain our previously created docker image:

```
docker images
```

You should see something similar to:

```
REPOSITORY                    TAG                 IMAGE ID            CREATED             SIZE
localhost:5000/my-container   1.2.3               8ed7b98760f0        12 minutes ago      15.5MB
```

Let's take a closer look by executing the following:

```
docker inspect localhost:5000/my-container:1.2.3
```

You will notice at the top of the json array outputted the following:

```
{
    "Id": "sha256:a3c7faef9e0c073d0e98a98ed0d2e3f5d4efd12729d4ceb7cc95ba5beb32ab65",
    "RepoTags": [
        "localhost:5000/my-container:1.2.3"
    ],
```

The above shows use the fact our image has one tag.

If you scroll further down you will see the following:

```
"ExposedPorts": {
                "80/tcp": {}
            }
```

The above shows us that the container exposes port 80.

Further down you will see the following:

```
"Author": "Steve Wade <swade@apprenda.com>",
```

The above is added due to the fact that our Dockerfile contained the `MAINTAINER` metadata tag.

At the very bottom of the output you will see the following:

```
"RootFS": {
            "Type": "layers",
            "Layers": [
                "sha256:16174e87921f30e394dbcde33e3b9939b81c461bbb7b501dacec9869880f4297",
                "sha256:9a993208f0b099bcbcbf05e259889a7b49709b55741595adcd4f5894c019b319",
                "sha256:723c6421bcfc62af4478871d31ecb777f0ab1e31ce6de6b749d14e109d116d19",
                "sha256:6f403372b09b01cfb6a82c45731c59b987fcf6815698ee34c31509eb3fd2912d",
                "sha256:cced18a10238bb44fddc8d65f6d76d78aab3c4e2de8e145e0198b241d62e325a"
            ]
        },
```

The above states that 5 image layers are used to construct this docker image.

Note: We will go into more detail above the output of `docker inspect` during the Docker 201 labs.

## Inspecting a running container.

Firstly execute the following command to obtain our running container:

```
docker ps -a
```

You should see something similar to:

```
CONTAINER ID        IMAGE                               COMMAND                  CREATED             STATUS              PORTS                    NAMES
38ca06faf7f6        localhost:5000/my-container:1.2.3   "nginx -g 'daemon ..."   4 minutes ago       Up 4 minutes        0.0.0.0:9999->80/tcp     my-container
83c7f820be98        registry:2                          "/entrypoint.sh /e..."   15 minutes ago      Up 15 minutes       0.0.0.0:5000->5000/tcp   registry
```

Let's take a closer look by executing the following (WARNING: The above outputs quite a long JSON array):

```
docker inspect my-container
```

If you scroll down to the networking portion of the array you will see the following:

```
"Ports": {
            "80/tcp": [
                {
                    "HostIp": "0.0.0.0",
                    "HostPort": "8080"
                }
            ]
        },
```

The above shows us that the host port is 8080 but the container port is 80.

You can also query the container for specific information (e.g. the image used) by executing the following command:

```
docker inspect -f "{{json .Config.Image }}" my-container
```

The output should be:

```
"localhost:5000/my-container:1.2.3"
```
